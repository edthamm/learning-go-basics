package main

import (
	"os"
	"testing"
)

func TestNewDeck(t *testing.T) {
	d := newDeck()

	if len(d) != 52 {
		t.Errorf("Expected deck length of 52, but go %v", len(d))
	}
}

func TestFirstCardIsAceOfSpades(t *testing.T) {
	d := newDeck()

	if d[0] != "Ace of Spades" {
		t.Errorf("Expected first card to be \"Ace of Spades\" but got %v", d[0])
	}
}

func TestLastCardIsKingOfClubs(t *testing.T) {
	d := newDeck()

	if d[len(d)-1] != "King of Clubs" {
		t.Errorf("Expected last card to be \"King of Clubs\" but got %v", d[len(d)-1])
	}
}

func TestSaveDeckToFileAndNewDeckFromFile(t *testing.T) {
	filename := "_decktesting"

	os.Remove(filename)

	deck := newDeck()

	deck.writeFile(filename)

	loadedDeck := newDeckFromFile(filename)

	if len(loadedDeck) != 52 {
		t.Errorf("Expected loadedeck length of 52, but go %v", len(loadedDeck))
	}

	os.Remove(filename)
}
