package main

import (
	"learning_go/microservice/mvc/app"
)

func main() {
	app.StartApp()
}
