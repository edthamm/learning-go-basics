package app

import (
	"learning_go/microservice/mvc/controllers"
	"log"
	"net/http"
)

func StartApp() {

	http.HandleFunc("/users", controllers.GetUser)

	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatal(err)
	}
}
